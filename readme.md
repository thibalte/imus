# ImUs

Design & Concept: Milk & Kairos  
Code: Thibault Brevet, Haris Kahn, Nathan Vogel

# Building

`$ npm install pug-cli -g`  
`$ pug src/pug --out build --watch`

Building js/css assets done with Koala, any build workflow will work to push them from /src to /build.

# TODO

- fix hsl/rgb/hex conversion ranges
- location select page
- email submit
- token validation in backend
- if pages/routes/controllers complexity keeps on going, good to migrate from jQuery/Page.js to a proper frontend framework, Vue/React