/* jshint asi:true */

console.log('********')
console.log('* ImUs *')
console.log('********')
console.log('concept & design: Milk & Kairos')
console.log('code: Thibault Brevet, Haris Kahn & Nathan Vogel')

$(function(){

	var splashed = false
	var h = 0
	var pick = ''

	// Create the background manager object.
	var background = new window.ImUsBackground({
	  maxDomNodeCount: 20,
	  canvas: $('#background').get(0)
	})

	page('*', function(ct, next){
		console.log('transition', splashed)
		$('#cursor').children().hide()
		if (!splashed) $('body').addClass('splashing')
		else $('body').removeClass('splashing')
		next()
	})

	/* ROOT */

	page('/', function(){
		console.log('home')

		if (!splashed) {
			$('#splash').fadeIn(250)
			$('.cursor-enter').show()
		} else $('.cursor-write').show()
	})

	$('#splash').click(function(){
		splashed = true
		$('body').removeClass('splashing')
		$(this).fadeOut()
		$('#helper').animate({opacity: 1}, 250)
		$('#cursor').children().hide()
		$('.cursor-write').show()
	})

	$('#comments').click(function(){
		page('/write')
	})

	/* ABOUT */

	page('/about', function(){
		console.log('about')

		$('body').addClass('splashing')
		$('.cursor-back').show()
		$('#about').fadeIn(250)
	})

	$('#about').click(function(){
		$(this).fadeOut(250)
		page('/')
	})

	/* WRITE */

	page('/write', function(){
		console.log('write')

		$('body').addClass('splashing')
		$('.cursor-picker').fadeIn(250)
		$('.input').text('')

		$('.step').hide()
		$('.step.picker').show()

		$('#background').fadeOut(250)
		$('#write').delay(250).fadeIn(250)
	})

	$('#write').click(function(){
		if ($('.picker').is(':visible')){

			var message = [{
				element: $('.step.draft .input').get(0),
				color: hslToRgb(h, 1, 0.5),
				draft: true,
			}]

			pick = hslToHex(h/Math.PI, 100, 50)

			console.log(message, pick, message.color)

			background.setMessages(message)

			$('.step').fadeOut(250)
			$('#background').fadeIn(250)
			$('.draft').delay(250).fadeIn(250)

			setTimeout(function() {
				$('.input').get(0).focus()
			}, 500)

			$('#cursor').children().hide()
			$('.cursor-send').show()

		} else if ($('.draft').is(':visible')){
			$('.draft').fadeOut(250)
			$('.location').delay(250).fadeIn(250)

			$('#cursor').children().hide()
			$('.cursor-skip').show()
		} else {

			var c = {
				content: $('.input').text(),
				color: pick,
				draft: false,
				country: 'ch'
			}

			console.log(c)

			// $.post('https://im-us.com/api/posts', c)

			$('#write').fadeOut(250)
			page('/')
		}
	})

	page()

	// UI

	$('#cursor').fadeIn(2000)

	$(window).mousemove(function(e){

		var ww = $(window).width()
		var wh = $(window).height()
		var x = e.pageX - ww/2
		var y = e.pageY-$(window).scrollTop() - wh/2
		h = Math.atan2(y, x) + Math.PI

		$('#cursor').css('left', e.pageX+'px')
		$('#cursor').css('top', e.pageY-$(window).scrollTop()+'px')

		$('.cursor-picker').css('background-color', 'hsl('+h*57.2+',100%,50%')
	})

	$('a').mouseenter(function(){
		$('#cursor').css('opacity', 0)
	})

	$('a').mouseleave(function(){
		$('#cursor').css('opacity', 1)
	})

	$('.input').keyup(function(){
		var l = $(this).text().length
		$(this).css('font-size', ((-l+140)*0.5+70)+'px')

		if (l > 140) {
			console.log('snip')
			$(this).text($(this).text().substring(0, 140))
		}
	})

	// COMMENTS

	var comments = []
	var pointer = ''

	pollComments()

	function pollComments(){
		$.get('https://im-us.com/api/posts?limit=50'+(pointer?'&sortKey=1&id='+pointer:''), function(data){

			console.log(data)

			if (data && data.posts && data.posts.length) {
				handleComments(data.posts)
				pointer = data.posts[0].id
			}

			setTimeout(function(){
				pollComments()
			}, 2000)
		})
	}

	function handleComments(c){

		var messages = []

		for (var i=c.length-1; i>=0; i--){

			$comment = $('<div class="comment">')
			$comment.css('opacity', 0)

			$content = $('<div class="comment-content">')
			$content.text(c[i].content)
			$content.css('font-size', ((-c[i].content.length+140)*0.5+70)+'px')

			// $canvas = $('<div class="comment-canvas">')
			// $canvas.css('background-color', obj.color)

			console.log(c[i].content)

			$comment.prepend($content)
			// $comment.append($canvas)

			var rgb = hexToRgb(c[i].color)

			messages.push({
				element: $comment.get(0),
				color: [rgb.r/255, rgb.g/255, rgb.b/255],
				draft: false
			})

			$('#comments').prepend($comment)
			$comment.animate({opacity: 1}, 1000)
		}

		background.setMessages(messages)
		$('#background').fadeIn(2000)
	}

	// https://stackoverflow.com/questions/2353211/hsl-to-rgb-color-conversion
	// [0...1] for all inputs
	function hslToRgb(h, s, l){
		var r, g, b

		if (s == 0) {
			r = g = b = l // achromatic
		} else {
			var hue2rgb = function hue2rgb(p, q, t) {
				if(t < 0) t += 1
				if(t > 1) t -= 1
				if(t < 1/6) return p + (q - p) * 6 * t
				if(t < 1/2) return q
				if(t < 2/3) return p + (q - p) * (2/3 - t) * 6
				return p
			}

			var q = l < 0.5 ? l * (1 + s) : l + s - l * s
			var p = 2 * l - q
			r = hue2rgb(p, q, h + 1/3)
			g = hue2rgb(p, q, h)
			b = hue2rgb(p, q, h - 1/3)
		}

		return [r,g,b]
	}

	// https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
	// #RGB, #RRGGBB input
	function hexToRgb(hex) {
		// Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
		var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i
		hex = hex.replace(shorthandRegex, function(m, r, g, b) {
			return r + r + g + g + b + b
		})

		var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
		return result ? {
			r: parseInt(result[1], 16),
			g: parseInt(result[2], 16),
			b: parseInt(result[3], 16)
		} : null
	}

	// https://stackoverflow.com/questions/36721830/convert-hsl-to-rgb-and-hex
	// [0...360], [0...100], [0...100]
	function hslToHex(h, s, l) {
		h /= 360
		s /= 100
		l /= 100
		var r, g, b
		if (s === 0) {
			r = g = b = l // achromatic
		} else {
			var hue2rgb = function(p, q, t) {
				if (t < 0) t += 1
				if (t > 1) t -= 1
				if (t < 1 / 6) return p + (q - p) * 6 * t
				if (t < 1 / 2) return q
				if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6
				return p
			}
			var q = l < 0.5 ? l * (1 + s) : l + s - l * s
			var p = 2 * l - q
			r = hue2rgb(p, q, h + 1 / 3)
			g = hue2rgb(p, q, h)
			b = hue2rgb(p, q, h - 1 / 3)
		}
		var toHex = function(x) {
			var hex = Math.round(x * 255).toString(16)
			return hex.length === 1 ? '0' + hex : hex
		}
		return '#'+toHex(r)+toHex(g)+toHex(b)
	}
})